package cn.tedu.sp02.controller;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.service.ItemService;
import cn.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@RestController
@Slf4j
public class ItemController {
    @Autowired
    private ItemService itemService;

    @GetMapping("/{orderId}")
    public JsonResult<List<Item>> getItem(@PathVariable String orderId) throws InterruptedException {
        List<Item> items = itemService.getItems(orderId);
        if (Math.random()<0.9){
            int t = new Random().nextInt(5000);
            System.out.println("超时："+t);
            Thread.sleep(t);
        }
        return JsonResult.build().code(200).data(items);
    }
    @PostMapping("/decreaseNumber")
    public JsonResult<?> decreaseNumber(@RequestBody List<Item> items){
        itemService.decreaseNumber(items);
        return JsonResult.build().code(200).msg("s减少成功");
    }
    @GetMapping("/favicon.ico")
    public void ico() {
    }
}
