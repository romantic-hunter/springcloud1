package cn.tedu.sp02.service;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.service.ItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ItemServiceImpl implements ItemService {
    @Override
    public List<Item> getItems(String orderId) {
        log.info("获取商品列表，orderId="+orderId);
        ArrayList<Item> list = new ArrayList<>();
        list.add(new Item("商品1",1,3));
        list.add(new Item("商品2",2,3));
        list.add(new Item("商品3",3,3));
        list.add(new Item("商品4",4,3));
        list.add(new Item("商品5",5,3));
        return list;
    }

    @Override
    public void decreaseNumber(List<Item> items) {
        for (Item item :items) {
            log.info("减少库存："+item);
        }

    }
}
