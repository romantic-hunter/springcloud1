package cn.tedu.sp06.filter;

import cn.tedu.web.util.JsonResult;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;


@Component
public class AccessFilter extends ZuulFilter {
    @Override
    public String filterType() {
        //return  "pre" ;
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return 6;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext currentContext = RequestContext.getCurrentContext();
        String serviceId = (String)currentContext.get(FilterConstants.SERVICE_ID_KEY);
        return "item-service".equals(serviceId);
    }

    @Override
    public Object run() throws ZuulException {
        //http://localhost:3001/item-service/y45tr3432r?token=7yu4t53r
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
        String token = request.getParameter("token");
        if (StringUtils.isBlank(token)){
            currentContext.setSendZuulResponse(false);
            currentContext.addZuulResponseHeader( "Content-Type" ,
                    "application/json;charset=UTF-8");
            String json = JsonResult.build().code(400)
                    .msg("未登录").toString();
            currentContext.setResponseBody(json);
        }
        return null;
    }
}
