package cn.tedu.sp04order.controller;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.Order;
import cn.tedu.sp01.pojo.User;
import cn.tedu.sp01.service.OrderService;
import cn.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;


    @GetMapping("/{orderId}")
    public JsonResult<Order> getOrder(@PathVariable String orderId){
        Order order = orderService.getOrder(orderId);
        return JsonResult.build().code(200).data(order);
    }

    @GetMapping("/create")
    public JsonResult<?> create(){
        User user = new User( 8,null, null);
        List<Item> items = new ArrayList<>();
        items.add(new Item("商品1",1,1));
        items.add(new Item("商品2",2,2));
        items.add(new Item("商品3",3,3));
        Order order = new Order("1235", user, items);
        orderService.create(order);
        return JsonResult.build().code(200).msg("创建订单成功");
    }
    @GetMapping("/favicon.ico")
    public void ico() {
    }

}
